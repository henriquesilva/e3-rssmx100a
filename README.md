# e3-rssmx100a

Wrapper for the module rssmx100a-epics-ioc

This module allows remote control of Rhode&Schwarz RF Generators from the SMX100A family (SMA100A, SMB100A and SMC100A)

## EPICS dependencies

```sh
$ make dep
STREAM_DEP_VERSION = 2.8.22+0
require rssmx100a,1.0.0+0
< configured ...
```

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

Remember to load the proper iocsh file for the device you are using (SMA100A, SMB100A or SMC100A). See `st/example.cmd`

```sh
$ iocsh cmds/example.cmd
```

For a SMC device, one would change in its cmd file
```
iocshLoad(${rssmx100a_DIR}rssma100a.iocsh, "P=${P},R=${R},IP_ADDR=${IP_ADDR}")
```
to
```
iocshLoad(${rssmx100a_DIR}rssmc100a.iocsh, "P=${P},R=${R},IP_ADDR=${IP_ADDR}")
```

## Contributing

Contributions through pull/merge requests only.
